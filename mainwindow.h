#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QObject>
#include <QStringList>
#include <QVector>
#include <QString>
#include <QDebug>
#include <algorithm>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

private slots:
	void on_SEdit_textChanged();
	void on_DEdit_textChanged();

private:
	Ui::MainWindow *ui;
	QString	S;
	QVector<QString> D;
	QVector<QString> foundWords;

	void	extractSData();
	void	extractDData();
	void	findWord();
	void	showResult();
	void	clearResult();
};

#endif // MAINWINDOW_H
