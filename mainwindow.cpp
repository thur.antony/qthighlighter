#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::on_SEdit_textChanged()
{
	clearResult();
	extractSData();
	if (!S.isEmpty() && !D.isEmpty())
		findWord();
}

void MainWindow::on_DEdit_textChanged()
{
	clearResult();
	extractDData();
	if (!S.isEmpty() && !D.isEmpty())
		findWord();
}

void MainWindow::extractSData()
{
	S = ui->SEdit->toPlainText();
	//qDebug() << "S: " << S;
}

void MainWindow::extractDData()
{
	QStringList wordList = ui->DEdit->toPlainText().split(' ', QString::SkipEmptyParts);
	D.clear();
	for (QString &word : wordList) {
		D.append(word);
	}
	auto	sortBySize = [] (const QString &s1, const QString &s2) -> bool
	{
		return s1.size() > s2.size();
	};
	std::sort(D.begin(), D.end(), sortBySize);
	//qDebug() << "D: " << D;
}

void MainWindow::findWord()
{
	foundWords.clear();
	for (int i{0}; i < D.size(); i++) {
		if (S.indexOf(D.at(i), 0, Qt::CaseInsensitive) != -1) {
			//qInfo() << D.at(i);
			foundWords.append(D.at(i));
			if (i + 1 < D.size() && D.at(i).size() > D.at(i + 1).size())
				break;
		}
	}
	showResult();
}

void MainWindow::showResult()
{
	if (foundWords.isEmpty()) {
		clearResult();
	}
	else {
		QString tmp{};
		for (int i{0}; i < foundWords.size(); i++) {
			if (i == 0) {
				//ui->highlight->setText(S);
				QString	content = S;
				content.replace(QRegExp(foundWords.at(i), Qt::CaseInsensitive),
								"<span style=\"background-color:yellow\">" + foundWords.at(i) + "</span>");
				ui->highlight->setText(content);
			}
			tmp += foundWords.at(i) + " ";
		}
		ui->result->setText("Result: " + tmp);
	}
}

void MainWindow::clearResult()
{
	ui->result->setText("");
	ui->highlight->setText("");
}
