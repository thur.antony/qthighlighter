# QtHighlighter

## Usage

In program you have two fields: the first one is for S string,
in terms of technical description, and the other one is for 
words that need to be searched, separated by spaces.

The result is displayed in two different ways: all matched longest
words and the first matched word, highlighted in S string.

## Tests:

S: HelloKiTTyponyKonyBatman

D: hello batman

---

S: zxcvbnappolloqwerty

D: appo pollo apopllo appollo longlongword